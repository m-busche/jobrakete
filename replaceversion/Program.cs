﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace replaceversion
{
    class Program
    {
        static void Main(string[] args)
        {
            string sourceFilename = args[0];
            string destinationFilename = args[1];
            // string exe = @"..\..\..\..\MediHomeOffice\bin\x64\Release\netcoreapp3.1\MediHomeOffice.exe";
            string exe = @".\MediHomeOffice\bin\x64\Release\netcoreapp3.1\MediHomeOffice.exe";
            try
            {
                var versionInfo = FileVersionInfo.GetVersionInfo(exe);
                string version = versionInfo.FileVersion;
                //StreamReader file = new StreamReader("version.txt");
                //string version = file.ReadLine();
                Console.WriteLine($"Source file: {sourceFilename}");
                Console.WriteLine($"Version: {version}");
                string today = $"{DateTime.Today:dd-MM-yyyy}";
                string text = File.ReadAllText(sourceFilename);
                text = text.Replace("<version>", version).Replace("<date>", today);
                File.WriteAllText(destinationFilename, text);
                Console.WriteLine($"Destination file {destinationFilename} written");
            }
            catch { }
        }
    }
}
