﻿rem Build 32- und 64 Bit
rem MSBuild .\MediHomeOffice.sln -t:Rebuild -p:Platform="x86" -p:Configuration=Release
MSBuild .\MediHomeOffice.sln -p:Platform="x86" -p:Configuration=Release
rem MSBuild .\MediHomeOffice.sln -t:Rebuild -p:Platform="x64" -p:Configuration=Release
MSBuild .\MediHomeOffice.sln -p:Platform="x64" -p:Configuration=Release
rem Version auslesen
rem .\MediHomeOffice\bin\x64\Release\netcoreapp3.1\MediHomeOffice.exe
rem Version speichern
rem notepad version.txt
rem Version in Download Link eintragen
.\replaceversion\bin\Release\netcoreapp3.1\replaceversion.exe .\README.md .\artefacts\README.md
rem Version in info.json eintragen
.\replaceversion\bin\Release\netcoreapp3.1\replaceversion.exe .\MediHomeOffice\info.json .\artefacts\info.json
rem Version in Innosetup eintragen
.\replaceversion\bin\Release\netcoreapp3.1\replaceversion.exe .\innosetup\setup.iss .\innosetup\setup_temp.iss
rem Version in Winscp Script eintragen
.\replaceversion\bin\Release\netcoreapp3.1\replaceversion.exe .\MediHomeOffice\winscp-script.txt .\artefacts\winscp-script.txt
rem html und pdf erstellen
pandoc .\artefacts\README.md -t html -o .\artefacts\index.html -s --toc --email-obfuscation=references
pandoc .\artefacts\README.md -o .\artefacts\JobRakete.pdf -s --toc
rem Copy PDF to build directories
copy .\artefacts\JobRakete.pdf .\MediHomeOffice\bin\x64\Release\netcoreapp3.1\
copy .\artefacts\JobRakete.pdf .\MediHomeOffice\bin\x86\Release\netcoreapp3.1\
rem Build setup
cd .\innosetup
"C:\Program Files (x86)\Inno Setup 6\ISCC.exe" setup_temp.iss
cd ..
rem Dateien hochladen
winscp.com /script=.\artefacts\winscp-script.txt