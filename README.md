﻿% JobRakete
% hallo@jobrakete.eu
% v<version> <date>

# JobRakete: Makes Your (Work-) Day

*JobRakete* ist ein Programm zur Vereinfachung der Login- und Logout- Prozesse im Home-Office und zur 
Erfassung deiner Arbeits- und Pausenzeiten. Alle wesentlichen Funktionen sind über drei Buttons erreichbar.

Beide Funktionen (Home-Office und Arebitszeit-Protokollierung) sind unabhängig voneinander oder 
gemeinsam nutzbar.

![JobRakete](screenshots/JobRakete_Home_mit_Balloons.png)

## Was ist JobRakete

- *JobRakete* kann deine täglichen Arbeits- und Pausenzeiten erfassen.
- Optional kann es Arbeits- und Pausenzeiten dauerhaft protokollieren.
- Im Home-Office kann es (optional):
  - Die LANCOM- (NCP) oder Shrew- VPN Client Verbindung steuern: Aufbau, Abbau, laufende Verbindungsüberwachung.
  - Deinen Swyx!It Client steuern (Abmeldung bei Pausenbeginn, Anmeldung bei Pausenende).

## Was ist JobRakete NICHT

***JobRakete* ersetzt nicht deine offizielle Arbeitszeitverwaltung!** Ich habe das Programm in meiner Freizeit entwickelt, 
damit es mir hilft, in den Arbeitstag zu starten, ohne, dass ich mir groß Gedanken um Arbeits- und Pausenzeiten 
machen müsste. Wenn du dir Sorge darum machst, dass dein Arbeitgeber Einblick in deine Aufzeichnungen bekommt, kannst 
du einfach die Zeit-Protokollierung abschalten. Dann werden keine Aufzeichnungen dauerhaft gespeichert.

## Voraussetzungen

- Microsoft Windows 7 oder neuer, 32 oder 64 Bit
- Microsoft .Net Core Framework für Desktop Applikationen, Version 3.1

Das .Net Core Framework wird im Laufe der Installation bei Bedarf vom Setup-Programm mit installiert.

## Installation

Die neueste Version von *JobRakete* kann [hier](https://jobrakete.eu/jobrakete/files/setupJobRakete_<version>.exe) herunter geladen werden. 
Die Installation wird durch Starten des Setup-Programms `setup_Jobrakete_<version>.exe` durchgeführt.

Da das Setup-Programm nicht digital signiert ist, und darüber hinaus das von vielen Sicherheitsprogrammen als 
**potenziell** schadhaft eingestufte Programm [AutoHotkey](https://www.autohotkey.com/) 
([Is it Safe?](https://safeweb.norton.com/report/show?url=autohotkey.com/download)) enthält, musst 
du dich möglicherweise durch mehrere Warndialoge durchklicken, bevor die Installation startet:

![Download-Warnung, Beispiel Google Chrome](screenshots/Jobrakete_Downloadwarnung.png)

<table>
  <tr>
    <td> <img src="screenshots/Jobrakete_Setup_Warnung1.png" alt="Windows 10 Warnung" width = 360px height = 360px ></td>
    <td> <img src="screenshots/Jobrakete_Setup_Warnung2.png" alt="Windows 10 Warnung" width = 360px height = 360px ></td>
   </tr> 
<table>

Windows Warnungen beim Start der Installation

[Virustotal Scan-Ergebnis](https://www.virustotal.com/gui/file/1cde3e91d109cdc8b676e73b4366a08ce4e954ff55f82a0fa8186ea9f65f5f91/detection) 
für `setupJobRakete_1.0.1.23exe`. Bei Bedarf kannst du die heruntergeladene Setup-Datei bei [Virustotal](https://www.virustotal.com) 
hochladen, um sie noch einmal überprüfen zu lassen.


## Einstellungen

Je nach dem, in welchem Szenario du *JobRakete* benutzen willst, musst du ein paar wesentliche Einstellungen vornehmen:

### Allgemein

![Allgemeine Einstellungen](screenshots/JobRakete_Settings_Allgemein.png)

**Hinweis**
Der Einstellungsdialog hat keinen *Speichern* Knopf; Sämtliche Änderungen werden beim Schließen des Dialogs gespeichert.

---

#### Für alle Szenarien

Stelle mit den Schaltern am unteren Rand folgende allgemeinen Einstellungen ein:

- *Swyx!It* starten: Startet den *Swyx* Client nachdem du die Arbeit begonnen hast. Sorgt auch dafür, dass dein *Swyx* in der Pause abgemeldet und nach der Pause wieder angemeldet wird. Achte darauf, dass Swyx dann nicht per Autostart von Windows gestartet werden sollte.
- *RDP starten*: Startet nach Arbeitsbeginn eine Remotedesktopsitzung zu dem Server/PC, der bei *IP Adresse Verbindungstest* eingetragen ist.
- *Zeitprotokoll*: Deine täglichen Arbeits- und Pausenzeiten werden dauerhaft gespeichert (pro Tag eine Datei). **Wenn der Schalter deaktiviert ist, werden lediglich die Zeiten des laufenden Tages gespeichert und am nächsten Tag überschrieben**.
- *Sounds abspielen*: Es werden Sounds bei Beginn und Ende von Arbeits- und Pausenzeiten abgespielt.

#### Szenario 1, Home-Office mit VPN-Client

- Wähle im Auswahlfeld *VPN Manager* deine VPN Client-Software aus.
- Klicke anschließend auf *Durchsuchen* rechts neben dem Feld *Pfad zu VPN Manager* und wähle die entprechende Datei für den *Shrew*- oder *LANCOM*- Client aus.
- Trage im Feld *Profilname* den Namen der VPN-Verbindung ein, zu der die Verbindung aufgebaut werden soll.
- Klicke auf *Durchsuchen* rechts neben dem Feld *Pfad zu Swyx* und wähle die Datei zum Starten von Swyx!It aus. Achte darauf, dass Swyx dann nicht per Autostart von Windows gestartet werden sollte.
- Trage im Feld *IP Adresse Verbindungstest* die IP-Adresse ein, mit der die Verbindung überwacht wird. Diese Adresse ist zugleich die Adresse des Servers oder PCs, zu dem du dich mit dem *Remotedesktopclient* verbinden lassen kannst.

![Beispieleinstellungen Home-Office mit Shrew VPN-Client](screenshots/JobRakete_Settings_Allgemein_Shrew.png)

#### Szenario 2, Home-Office ohne VPN-Client bzw. LAN-Modus

- Wähle im Auswahlfeld *VPN Manager* *Routerverbindung oder LAN-Modus*.
- Klicke auf *Durchsuchen* rechts neben dem Feld *Pfad zu Swyx* und wähle die Datei zum Starten von Swyx!It aus.
- Trage im Feld *IP Adresse Verbindungstest* die IP-Adresse ein, über die die Verbindung überwacht wird. Diese Adresse ist zugleich die Adresse des Servers oder PCs, zu dem du dich mit dem *Remotedesktopclient* verbinden lassen kannst.

Deaktiviere gegebenenfalls die Schalter *Swyx!It starten* und *RDP starten*, wenn du nicht möchtest, dass *JobRakete* sich darum kümmert bzw. falls du *JobRakete* nur als Zeitverwaltung benutzen möchtest.

![Beispieleinstellungen LAN Modus](/JobRakete_Settings_Allgemein_LAN.png)

### Arbeitszeit

Im Reiter *Arbeitszeit* kannst du deine täglichen Sollzeiten eintragen. Voreingestellt sind 8 Stunden / Tag Montags bis Freitags. 
Wenn deine Arbeitszeiten davon abweichen, kannst du das hier anpassen. Beachte: **Änderungen wirken sich nicht auf eine 
bereits begonnene Schicht aus**! Passe also die Zeiten an, bevor du deinen Arbeitstag beginnst.

Als Pausenzeit wird eine Stunde pro Tag angenommen (nicht konfigurierbar).

![Arbeitszeiteinstellung](screenshots/JobRakete_Settings_Arbeitszeit.png)

### Mattermost

Wenn du möchtest, sendet *JobRakete* bei bestimmten Ereignissen Nachrichten an einen *Mattermost* Kanal. Diese Ereignisse sind:

- Beginn der Arbeitszeit (fester Text)
- Beginn einer Pause (freier Text)
- Ende einer Pause (fester Text)
- Ende der Arbeitszeit (fester Text)

Bitte fülle die Felder *Kanal* und auch *Dein Vorname* entsprechend aus, ansonsten ist den Nachrichten nicht zu entnehmen, von wem sie gesendet wurden.

**Der Versand von *Mattermost* Nachrichten ist standardmäßig deaktivert**. Wenn du den Versand aktivierst, wirst du in einen 
privaten Kanal eingeladen, in dem nur *JobRakete* - Benutzer mitlesen können.

![Mattermost Einstellungen](screenshots/JobRakete_Settings_Mattermost.png)

![Mattermost Pausen-Nachricht](screenshots/JobRakete_Settings_Mattermost_Pause-Start.png)

![*JobRakete* Nachrichten](screenshots/Mattermost_Example.png)

### Theme

Im Reiter *Theme* kannst du das Aussehen von *JobRakete* verändern. Du hast die Wahl zwischen 23 Farbtönen in jeweils 
zwei Akzenten (*Dark* oder *Light*). Die Änderungen wirken sich sofort aus.

![Theme Einstellungen](screenshots/JobRakete_Settings_Theme.png)


## Bedienung

Die Bedienung von *JobRakete* ist so einfach wie möglich gehalten, das Programm soll dir ja Arbeit abnehmen und nicht nerven.

### Beginn des Arbeitstages

Klicke auf den linken *Rakete* Button, um deine Arbeitszzeit zu beginnen. Es beginnt die Arbeitszeiterfassung. 
Je nach Einstellung wird anschließend auch die VPN-Verbindung aufgebaut und *Swyx!It* gestartet sowie die 
Remotedesktop-Verbindung aufgebaut.

Die VPN-Verbindung wird dauerhaft im Hintergrund überprüft. Bei einem Verbindungsabbruch werden drei Versuche 
im Abstand einer Minute unternommen, die Verbindung wieder herzustellen.

### Pausenaufzeichnung

Der mittlere *Pause*-Button leitet eine Pause ein und beendet sie beim zweiten Klick wieder. Beim Beenden der Pause 
wird automatisch die Arbeitszeit wieder aufgenommen.

Klicke auf den *Pause* Button, um eine Pause einzulegen. Die Arbeitszeitaufzeichnung wird pausiert und die 
Pausendauer wird aufgezeichnet. Der Rand des Buttons wird ein wenig breiter, damit du auf einen Blick erkennst, 
dass du im Pausenmodus bist. Je nach Einstellung wird dein *Swyx!It* Client während der Pause abgemeldet.

Klicke am Ende der Pause erneut auf den *Pause* Button, um die Arbeitszeit forzusetzen. Der *Swyx!It* Client 
wird dann automatisch wieder angemeldet.

**Hinweis**

Du kannst im Laufe eines Tages beliebig viele Pausen einlegen.

### Feierabend

Dein Lieblingsbutton wird der *Feierabend* Button werden! Beim ersten Klick auf den *Feierabend* Button wird dir 
eine Zusammenfassung deiner Arbeits- und Pausenzeiten angezeigt. Du hast dann die Möglichkeit, diese in deine 
Arbeitszeitverwaltung zu übertragen.

**Hinweis**

*Die folgenden Screenshots dienen nur als Beispiele und enthalten möglicherweise unsinnige Zeitangaben*

![Beispiel Tageszusammenfassung](screenshots/JobRakete_Home_Tageszusammenfassung.png)

Beim zweiten Klick auf den *Feierabend* Button wird deine VPN-Verbindung (falls konfiguriert) getrennt und der
*Swyx!It* Client beendet.

Falls du das Zeitprotokoll in den Einstellungen aktiviert hast, wird für diesen Arbeitstag eine Protokolldatei im 
*JSON* Format angelegt, in der detaillierte Informationen zu deinen Arbeits- und Pausenzeiten gespeichert werden.

### Sonderfall: Unvollständige Aufzeichnung

*JobRakete* sollte im Idealfall während deines Arbeitstages möglichst dauerhaft im Hintergrund laufen, damit alle Zeiten 
vollständig aufgezeichnet werden können. Falls das Programm jedoch während deines Arbeitstages einmal beendet 
wurde (zum Beispiel, weil dein System einen Neustart brauchte, oder weil du zu einem Außendienst-Einsatz 
unterwegs warst), kannst du *JobRakete* auch erneut starten. Das Programm liest dann den minütlich gespeicherten 
Zwischenstand ein und bietet dir an, diesen zu laden. 

![Aufzeichnung laden](screenshots/JobRakete_Home_Aufzeichnung_fortsetzen.png)

In einem weiteren Dialog musst du entscheiden, ob die 
in der Zeit zwischen Programm-Ende und -Neustart vergangenen Zeit der Arbeits- oder der Pausenzeit zugeordnet 
werden soll. *Eine Aufteilung in anteilige Arbeits- und Pausenzeit ist nicht möglich*. Anschließend kannst du deine 
Arbeit fortsetzen (oder den Feierabend einläuten).

![Zwischenzeit zuordnen](screenshots/JobRakete_Home_Aufzeichnung_Zuordnung.png)

## Arbeitszeitprotokolle

Wenn du die Zeitprotokollierung in den Einstellungen aktiviert hast, wird pro Tag eine Datei mit allen 
Informationen über deinen Arbeitstag angelegt. Die Protokolldateien werden im Ordner *JobRakete* 
unter deinem *Dokumenten*-Verzeichnis angelegt. Der Menupunkt *Datei* - *Arbeitszeitprotokolle* öffnet 
den Ordner im Windows Dateiexplorer.

Die Protokolle werden im *JSON* Format gespeichert - die Dateien können mit jedem Textprogramm (Notepad, 
notfalls Word oder ähnliches) geöffnet und gelesen werden.

![Protokoll](screenshots/Arbeitszeitprotokoll.png)

Die für die Übertragung in deine Arbeitszeitverwaltung wichtigen Daten findest du hinter den Bezeichnungen 
`ShiftStart` (Arbeitszeitbeginn), `ShiftEnd` (Feierabend) und `TotalPauseTime` (gesamte Pausenzeit). 
Die Datei enthält natürlich auch sämtliche Details über Arbeits- und Pausensektionen, wenn du´s mal 
genauer wissen möchtest.

Eine vollständige Beispieldatei kannst du [hier](https://jobrakete.eu/JobRaketen-Protokoll_09-00-2021.json) 
herunterladen bzw. im Webbrowser öffnen.

**Hinweis**

Die Protokolldateien kannst du nach Belieben löschen, sie werden vom Programm nicht weiter benötigt.

## Lizenz

*JobRakete* steht unter der *DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE*, was bedeutet, dass du damit 
tun kannst, was du willst. Siehe auch `LIZENZ.TXT` im Installations-Ordner.

Der Quellcode des Programms steht derzeit nicht zur Verfügung.

### Lizenzen Dritter

*JobRakete* nutzt einige Bibliotheken Dritter, deren Lizenzbestimmungen hier aufgeführt sind:

| Bibliothek                         | Lizenz                                                          |
| ----------                         | ------                                                          |
| MahApps.Metro                      | [MIT](https://licenses.nuget.org/MIT)                           |
| MahApps.Metro.IconPacks.Material   | [MIT](https://licenses.nuget.org/MIT)                           |
| Newtonsoft.Json                    | [MIT](https://licenses.nuget.org/MIT)                           |
| Matterhook.NET.MatterhookClient    | [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.de.html)    |
| Serilog                            | [Apache-2.0](https://licenses.nuget.org/Apache-2.0)             |
| Serilog.Sinks.TextWriter           | [Apache-2.0](https://licenses.nuget.org/Apache-2.0)             |
| AutoHotkey                         | [GNU GENERAL PUBLIC LICENSE](https://www.gnu.org/licenses/#GPL) |


## Impressum

JobRakete ist ein privates Projekt, es verfolgt keine kommerziellen Interessen.

Verantwortlich für den Inhalt dieser Seite und das Programm *JobRakete*:

```
Markus Busche
Knorrstraße 16
D-24106 Kiel
```

[Kontakt](mailto:hallo@jobrakete.eu?subject=JobRakete)

## Datenschutzerklärung

Weder das Programm *JobRakete* noch diese Webseite sammelt Daten über die Benutzer oder sendet irgendetwas 
irgendwohin.

