﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using ControlzEx.Theming;
using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;

namespace MediHomeOffice
{
    /// <summary>
    /// Interaktionslogik für Settings.xaml
    /// </summary>
    public partial class Settings : MetroWindow
    {
        public Settings()
        {
            InitializeComponent();

            // Fülle Werte aus Settings
            if (cbVPNManager.SelectedIndex == 2)
            {
                btBrowseVPN.IsEnabled = false;
                txVPNClientExe.Text = "Nicht erforderlich";
            }
            string[] colors = { "Red", "Green", "Blue", "Purple", "Orange", "Lime", "Emerald", 
                "Teal", "Cyan", "Cobalt", "Indigo", "Violet", "Pink", "Magenta", "Crimson", 
                "Amber", "Yellow", "Brown", "Olive", "Steel", "Mauve", "Taupe", "Sienna" };
            foreach (string c in colors)
            {
                cbColor.Items.Add(c);
            }
            cbAccent.Items.Add("Light");
            cbAccent.Items.Add("Dark");

            // Allgemeine Einstallungen
            cbVPNManager.SelectedIndex = MainWindow.usersettings.VpnManager;
            txVPNClientExe.Text = MainWindow.usersettings.VpnClientExe;
            txVPNProfil.Text = MainWindow.usersettings.VpnProfileName;
            txSwyxExe.Text = MainWindow.usersettings.SwyxExe;
            txIPAdress.Text = MainWindow.usersettings.IP2Connect;
            tglSwyxstart.IsOn = MainWindow.usersettings.SwyxStart;
            tglRdpstart.IsOn = MainWindow.usersettings.RdpStart;
            tglWorklog.IsOn = MainWindow.usersettings.WorkLog;
            tglSound.IsOn = MainWindow.usersettings.SoundsEnabled;

            // Arbeitszeit
            nmMoh.Value = MainWindow.usersettings.SzMontagHh;
            nmMom.Value = MainWindow.usersettings.SzMontagMm;
            nmDih.Value = MainWindow.usersettings.SzDienstagHh;
            nmDim.Value = MainWindow.usersettings.SzDienstagMm;
            nmMih.Value = MainWindow.usersettings.SzMittwochHh;
            nmMim.Value = MainWindow.usersettings.SzMittwochMm;
            nmDoh.Value = MainWindow.usersettings.SzDonnerstagHh;
            nmDom.Value = MainWindow.usersettings.SzDonnerstagMm;
            nmFrh.Value = MainWindow.usersettings.SzFreitagHh;
            nmFrm.Value = MainWindow.usersettings.SzFreitagMm;
            nmSah.Value = MainWindow.usersettings.SzSamstagHh;
            nmSam.Value = MainWindow.usersettings.SzSamstagMm;
            nmSoh.Value = MainWindow.usersettings.SzSonntagHh;
            nmSom.Value = MainWindow.usersettings.SzSonntagMm;

            // Mattermost
            tgMattermostActive.IsOn = MainWindow.usersettings.MmActivated;
            txWebhook.Text = MainWindow.usersettings.MmWebhook;
            txPublicChannel.Text = MainWindow.usersettings.MmChannel;
            txVorname.Text = MainWindow.usersettings.MmUserName;

            // Theme
            cbColor.SelectedItem = MainWindow.usersettings.ThemeColor;
            cbAccent.SelectedItem = MainWindow.usersettings.ThemeAccent;
        }

        private async void btBrowseVPN_Click(object sender, RoutedEventArgs e)
        {
            if (cbVPNManager.Text == "")
            {
                await this.ShowMessageAsync("Hinweis", "Bitte erst einen VPN-Manager auswählen.");
                return;
            }
            string pf = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
            OpenFileDialog dialog = new OpenFileDialog();

            if (cbVPNManager.Text == "Shrewsoft")
            {
                dialog.InitialDirectory = pf + @"\ShrewSoft\VPN Client";
                dialog.Filter = "Shrewsoft Client (ipsecc.exe)|ipsecc.exe|Alle Dateien (*.*)|*.*";
                dialog.FileName = "ipsecc.exe";
            }
            else if (cbVPNManager.Text == "LANCOM Client")
            {
                dialog.InitialDirectory = pf + @"\LANCOM\Advanced VPN Client";
                dialog.Filter = "LANCOM Client (ncpclientcmd.exe)|ncpclientcmd.exe|Alle Dateien (*.*)|*.*";
                dialog.FileName = "ncpclientcmd.exe";
            }
            else
            {
                await this.ShowMessageAsync("Hinweis", "Funktion abgebrochen.");
            }
            dialog.Title = "Ausführbare Datei für Shrewsoft oder LANCOM Client auswählen";
            dialog.CheckFileExists = true;

            if (dialog.ShowDialog() == true)
            {
                txVPNClientExe.Text = dialog.FileName;
                // Properties.Settings.Default.VPNClientExe = dialog.FileName;
            }
        }

        private void btBrowseSwyx_Click(object sender, RoutedEventArgs e)
        {
            String pf = Helpers.ProgramFilesx86();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = pf + @"\SwyxIt!";
            dialog.Filter = "Swyx!It Client (SwyxIt!.exe)|SwyxIt!.exe|Alle Dateien (*.*)|*.*";
            dialog.FileName = "SwyxIt!.exe";
            dialog.Title = "Ausführbare Datei für Swyx!It Client auswählen";
            if (dialog.ShowDialog() == true)
            {
                txSwyxExe.Text = dialog.FileName;
                // Properties.Settings.Default.SwyxExe = dialog.FileName;
            }
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (cbVPNManager.SelectedIndex < 2)
            {
                btBrowseVPN.IsEnabled = true;
                if (txVPNClientExe.Text == "Nicht erforderlich")
                {
                    txVPNClientExe.Text = "Pfad auswählen";
                }
            }

            // Properties.Settings.Default.VPNManager = cbVPNManager.SelectedIndex;
            if ((cbVPNManager.SelectedIndex == 0 && txVPNClientExe.Text.Contains("ncpclientcmd")) ||
                (cbVPNManager.SelectedIndex == 1 && txVPNClientExe.Text.Contains("ipsecc")))
            {
                txVPNClientExe.Text = "Pfad auswählen";
            }
            if (cbVPNManager.SelectedIndex == 2)
            {
                txVPNClientExe.Text = "Nicht erforderlich";
                btBrowseVPN.IsEnabled = false;
            }
        }

        private void winSettings_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Allgemein
            MainWindow.usersettings.VpnManager = cbVPNManager.SelectedIndex;
            MainWindow.usersettings.VpnClientExe = txVPNClientExe.Text;
            MainWindow.usersettings.VpnProfileName = txVPNProfil.Text;
            MainWindow.usersettings.SwyxExe = txSwyxExe.Text;
            MainWindow.usersettings.IP2Connect = txIPAdress.Text;
            MainWindow.usersettings.SwyxStart = tglSwyxstart.IsOn;
            MainWindow.usersettings.RdpStart = tglRdpstart.IsOn;
            MainWindow.usersettings.WorkLog = tglWorklog.IsOn;
            MainWindow.usersettings.SoundsEnabled = tglSound.IsOn;

            // Arbeitszeit
            MainWindow.usersettings.SzMontagHh = (int)nmMoh.Value;
            MainWindow.usersettings.SzMontagMm = (int)nmMom.Value;
            MainWindow.usersettings.SzDienstagHh = (int)nmDih.Value;
            MainWindow.usersettings.SzDienstagMm = (int)nmDim.Value;
            MainWindow.usersettings.SzMittwochHh = (int)nmMih.Value;
            MainWindow.usersettings.SzMittwochMm = (int)nmMim.Value;
            MainWindow.usersettings.SzDonnerstagHh = (int)nmDoh.Value;
            MainWindow.usersettings.SzDonnerstagMm = (int)nmDom.Value;
            MainWindow.usersettings.SzFreitagHh = (int)nmFrh.Value;
            MainWindow.usersettings.SzFreitagMm = (int)nmFrm.Value;
            MainWindow.usersettings.SzSamstagHh = (int)nmSah.Value;
            MainWindow.usersettings.SzSamstagMm = (int)nmSam.Value;
            MainWindow.usersettings.SzSonntagHh = (int)nmSoh.Value;
            MainWindow.usersettings.SzSonntagMm = (int)nmSom.Value;

            // Mattermost
            MainWindow.usersettings.MmActivated = tgMattermostActive.IsOn;
            MainWindow.usersettings.MmWebhook = txWebhook.Text;
            MainWindow.usersettings.MmChannel = txPublicChannel.Text;
            MainWindow.usersettings.MmUserName = txVorname.Text;

            // Theme
            MainWindow.usersettings.ThemeColor = cbColor.Text;
            MainWindow.usersettings.ThemeAccent = cbAccent.Text;
        }

        private void ChangeTheme()
        {
            if (cbColor.SelectedItem != null && cbAccent.SelectedItem != null)
            {
                string t = $"{cbAccent.SelectedItem}.{cbColor.SelectedItem}";
                ThemeManager.Current.ChangeTheme(Application.Current, t);
            }
        }
    }
}
