﻿using JsonSettings.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediHomeOffice
{
    public class UserSettings : SettingsBase
    {
        public int VpnManager { get; set; }
        public string SwyxExe { get; set; }
        public string IP2Connect { get; set; }
        public bool WorkLog { get; set; }
        public string VpnProfileName { get; set; }
        public string VpnClientExe { get; set; }
        public bool RdpStart { get; set; }
        public bool SwyxStart { get; set; }
        public bool SoundsEnabled { get; set; }
        public int SzMontagHh { get; set; }
        public int SzMontagMm { get; set; }
        public int SzDienstagHh { get; set; }
        public int SzDienstagMm { get; set; }
        public int SzMittwochHh { get; set; }
        public int SzMittwochMm { get; set; }
        public int SzDonnerstagHh { get; set; }
        public int SzDonnerstagMm { get; set; }
        public int SzFreitagHh { get; set; }
        public int SzFreitagMm { get; set; }
        public int SzSamstagHh { get; set; }
        public int SzSamstagMm { get; set; }
        public int SzSonntagHh { get; set; }
        public int SzSonntagMm { get; set; }
        public bool FirstStartEver { get; set; }
        public string MmChannel { get; set; }
        public string MmWebhookUser { get; set; }
        public string MmIconUrl { get; set; }
        public string MmWebhook { get; set; }
        public string MmUserName { get; set; }
        public bool MmActivated { get; set; }
        public string ThemeColor { get; set; }
        public string ThemeAccent { get; set; }

        public override string Filename => BuildPath(Environment.SpecialFolder.LocalApplicationData, "JobRaketeSettings.json");

        // Set default values
        public UserSettings()
        {
            String pf = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ProgramFiles);
            VpnManager = 0;
            SwyxExe = pf + @"\SwyxIt!\SwyxIt!.exe";
            IP2Connect = "192.168.0.202";
            WorkLog = false;
            VpnProfileName = "VPN Firma";
            VpnClientExe = pf + @"\ShrewSoft\VPN Client\ipsecc.exe";
            RdpStart = true;
            SwyxStart = true;
            SoundsEnabled = true;
            SzMontagHh = 8;
            SzDienstagHh = 8;
            SzMittwochHh = 8;
            SzDonnerstagHh = 8;
            SzFreitagHh = 8;
            SzMontagMm = 0;
            SzDienstagMm = 0;
            SzMittwochMm = 0;
            SzDonnerstagMm = 0;
            SzFreitagMm = 0;
            SzSamstagHh = 0;
            SzSamstagMm = 0;
            SzSonntagHh = 0;
            SzSonntagMm = 0;
            FirstStartEver = true;
            MmChannel = "Jobrakete";
            MmWebhookUser = "jobrakete-bot";
            MmIconUrl = "https://www.bustabitscript.com/wp-content/uploads/2020/07/cropped-rocket.png";
            MmWebhook = "https://mattermost.medisoftware.org/hooks/7usu5xfq5fgizkehxwgs1hfdno";
            MmUserName = string.Empty;
            MmActivated = false;
            ThemeColor = "Blue";
            ThemeAccent = "Light";
        }
    }
}
