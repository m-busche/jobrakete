﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediHomeOffice
{
    class TimeSection
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public TimeSpan Duration { get; set; }
        public string Comment { get; set; }

        public TimeSection()
        {
            Start = DateTime.MinValue;
            End = DateTime.MinValue;
            Duration = TimeSpan.Zero;
            Comment = string.Empty;
        }

    }
}
