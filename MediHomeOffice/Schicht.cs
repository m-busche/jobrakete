﻿using System;
using System.Collections.Generic;

namespace MediHomeOffice
{
    class Schicht
    {
        public DateTime ShiftStart { get; set; }
        public DateTime ShiftEnd { get; set; }
        public TimeSpan TotalWorkTime { get; set; }
        public TimeSpan TotalPauseTime { get; set; }
        public double WorkProgress { get; set; }
        public double PauseProgress { get; set; }
        public TimeSpan TargetWorkTime { get; set; }
        public TimeSpan WorkTimeRemaining { get; set; }
        public TimeSpan TargetPauseTime { get; set; }
        public TimeSpan PauseTimeRemaining { get; set; }
        public List<TimeSpan> PauseSectionsDurations { get; set; }
        public List<TimeSpan> WorkSectionsDurations { get; set; }
        public List<TimeSection> PauseSections { get; set; }
        public List<TimeSection> WorkSections { get; set; }
        public DateTime CurrentSectionStart { get; set; }
        public DateTime CurrentSectionEnd { get; set; }
        public TimeSpan CurrentSectionDuration { get; set; }
        public string CurrentSectionComment { get; set; }
        public bool isWorking { get; set; }
        public bool FeierabendClickedOnce { get; set; }
        public bool LoadedFromSnapshot { get; set; }

        public Schicht()
        {
            TimeSpan tsSoll = TimeSpan.Zero;
            DayOfWeek dow = DateTime.Now.DayOfWeek;
            switch (dow)
            {
                case DayOfWeek.Monday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzMontagHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzMontagMm);
                    break;
                case DayOfWeek.Tuesday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzDienstagHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzDienstagMm);
                    break;
                case DayOfWeek.Wednesday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzMittwochHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzMittwochMm);
                    break;
                case DayOfWeek.Thursday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzDonnerstagHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzDonnerstagMm);
                    break;
                case DayOfWeek.Friday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzFreitagHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzFreitagMm);
                    break;
                case DayOfWeek.Saturday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzSamstagHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzSamstagMm);
                    break;
                case DayOfWeek.Sunday:
                    tsSoll += TimeSpan.FromHours(MainWindow.usersettings.SzSonntagHh);
                    tsSoll += TimeSpan.FromMinutes(MainWindow.usersettings.SzSonntagMm);
                    break;
            }
            TargetWorkTime = tsSoll;
            isWorking = false;
            TargetPauseTime = TimeSpan.FromHours(1);
            WorkSectionsDurations = new List<TimeSpan>();
            PauseSectionsDurations = new List<TimeSpan>();
            WorkSections = new List<TimeSection>();
            PauseSections = new List<TimeSection>();
            FeierabendClickedOnce = false;
            WorkTimeRemaining = TargetWorkTime;
            PauseTimeRemaining = TargetPauseTime;
            TotalWorkTime = TimeSpan.Zero;
            TotalPauseTime = TimeSpan.Zero;
            // SchichtEnde = DateTime.MinValue;
        }
        /// <summary>
        /// Calculates derived values
        /// </summary>
        /// <param name="s"></param>
        public static void UpdateTimes(Schicht s)
        {
            DateTime now = DateTime.Now;
            s.CurrentSectionDuration = now - s.CurrentSectionStart;
            if (s.isWorking)
            {
                s.WorkTimeRemaining = s.TargetWorkTime.Subtract(s.WorkSectionsDurations.Sum()).Subtract(s.CurrentSectionDuration);
                // s.WorkTimeRemaining = s.TargetWorkTime - s.WorkSectionsDurations.Sum() - s.CurrentSektionDuration;
                s.TotalWorkTime = s.WorkSectionsDurations.Sum() + s.CurrentSectionDuration;
            }
            else
            {
                s.PauseTimeRemaining = s.TargetPauseTime.Subtract(s.PauseSectionsDurations.Sum()).Subtract(s.CurrentSectionDuration);
                // s.PauseTimeRemaining = s.TargetPauseTime - s.PauseSectionsDurations.Sum() - s.CurrentSektionDuration;
                s.TotalPauseTime = s.PauseSectionsDurations.Sum() + s.CurrentSectionDuration;
            }
            s.WorkProgress = Math.Round((s.TargetWorkTime - s.WorkTimeRemaining) / s.TargetWorkTime * 100, 2);
            s.PauseProgress = Math.Round((s.TargetPauseTime - s.PauseTimeRemaining) / s.TargetPauseTime * 100, 2);
        }

    public static void AddSektion(Schicht s)
        {
            DateTime now = DateTime.Now;
            if (s.isWorking)
            {
                s.WorkSections.Add(new TimeSection
                {
                    Start = s.CurrentSectionStart,
                    End = now,
                    Duration = s.CurrentSectionDuration,
                    Comment = s.CurrentSectionComment
                });
                s.WorkSectionsDurations.Add(s.CurrentSectionDuration);
            }
            else
            {
                s.PauseSections.Add(new TimeSection
                {
                    Start = s.CurrentSectionStart,
                    End = now,
                    Duration = s.CurrentSectionDuration,
                    Comment = s.CurrentSectionComment
                });
                s.PauseSectionsDurations.Add(s.CurrentSectionDuration);
            }
            // CurrentSektion zurücksetzen
            s.CurrentSectionStart = DateTime.Now;
            s.CurrentSectionEnd = DateTime.MaxValue;
            s.CurrentSectionDuration = TimeSpan.Zero;
        }
    }
}
