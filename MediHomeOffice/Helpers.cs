﻿using Newtonsoft.Json;
using MediHomeOffice.Properties;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Media;
using System.Net.NetworkInformation;
using System.Resources;
using System.Linq;
using System.Threading.Tasks;

namespace MediHomeOffice
{
    class Helpers
    {
#if DEBUG
        private static readonly string dataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\JobRakete.Debug";
        private static string snapShotFileName = $"{dataDirectory}\\snapshot.Debug.json";
#else
        private static readonly string dataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\JobRakete";
        private static string snapShotFileName = $"{dataDirectory}\\snapshot.json";
#endif

        public static string ProgramFilesx86()
        {
            if (8 == IntPtr.Size
                || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }
        /// <summary>
        /// Returns true if a host answers ping
        /// </summary>
        /// <param name="nameOrAddress"></param>
        /// <returns></returns>
        public static async Task<bool> PingHostAsync(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = null;

            try
            {
                for (int i=0; i<3; i++)
                {
                    pinger = new Ping();
                    PingReply reply = pinger.Send(nameOrAddress, 2000);
                    pingable = reply.Status == IPStatus.Success;
                    await Task.Delay(1000);
                    if (pingable) { break; }
                }
            }
            catch (PingException)
            {
                return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }
            return pingable;
        }

        /// <summary>
        /// Connects to peer (LANCOM or Shrew Client)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="profil"></param>
        /// <param name="exefile"></param>
        public static void ConnectVPN(string type, string profil, string exefile)
        {
            Process pVPN = new Process();
            pVPN.StartInfo.FileName = exefile;
            if (!File.Exists(exefile)) { return; }
            if (type == "s")
            {

                pVPN.StartInfo.Arguments = $"-r \"{profil}\" -a";
            }
            else if (type == "l")
            {
                pVPN.StartInfo.Arguments = $"/connect {profil} -a";
            }
            try
            {
                pVPN.Start();
            }
            catch { }
        }

        public static void StartProcess(string exefile, string args = "")
        {
            Process p = new Process();
            p.StartInfo.FileName = exefile;
            if (args != "")
            {
                p.StartInfo.Arguments = args;
            }
            try
            {
                p.Start();
            }
            catch { }
        }

        public static void StartShellProcess(string fileOrUri)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = fileOrUri,
                UseShellExecute = true
            };
            try
            {
                Process.Start(psi);
            }
            catch { }
        }

        /// <summary>
        /// Play sound from embedded resources
        /// </summary>
        /// <param name="soundCount"></param>
        public static void PlaySound(int soundCount)
        {
            ResourceSet resourceSet = Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            int i = resourceSet.Cast<object>().Count();
            if (soundCount > i) { return; }

            var resSound = Properties.Resources.start;

            switch (soundCount)
            {
                case 0:
                    resSound = Resources.start;
                    break;
                case 1:
                    resSound = Resources.pause;
                    break;
                case 2:
                    resSound = Resources.end;
                    break;
                case 3:
                    resSound = Resources.overdue;
                    break;
                case 4:
                    resSound = Resources.overdue2;
                    break;
            }

            SoundPlayer player = new SoundPlayer(resSound);
            player.Play();
        }

        /// <summary>
        /// Checks if the program has been started today
        /// </summary>
        /// <returns></returns>
        public static bool IsFirststartToday()
        {
#if DEBUG
            string f = $"{Path.GetTempPath()}\\.jobrakete.debug";
#else
            string f = $"{Path.GetTempPath()}\\.jobrakete";
#endif

            if (!File.Exists(f)) {
                File.Create(f).Dispose();
                return true;
            }
            else
            {
                DateTime cdate = File.GetLastAccessTime(f);
                if (cdate.Date == DateTime.Today.Date) { 
                    return false;
                }
                else
                {
                    File.Delete(f);
                    File.Create(f).Dispose();
                    return true;
                }
            }
        }

        /// <summary>
        /// Writes the given object instance to a Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [JsonIgnore] attribute.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToJsonFile<T>(T objectToWrite, bool append = false, bool backup = false) where T : new()
        {
            string filename;
            if (!Directory.Exists(dataDirectory))
            {
                try
                {
                    Directory.CreateDirectory(dataDirectory);
                }
                catch { }
            }
            if (backup)
            {
                filename = $"{dataDirectory}\\JobRaketen-Protokoll_{DateTime.Today:dd-MM-yyyy}.json";
            }
            else
            {
                filename = snapShotFileName;
            }
            TextWriter writer = null;
            try
            {
                var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite, formatting: Formatting.Indented);
                writer = new StreamWriter(filename, append);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        /// <summary>
        /// Reads an object instance from an Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the Json file.</returns>
        public static T ReadFromJsonFile<T>() where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(snapShotFileName);
                var fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(fileContents);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
        public static void ClientConnect(UserSettings userSettings)
        {
            if (userSettings.VpnManager == 0)
            {
                ConnectVPN("s", userSettings.VpnProfileName, userSettings.VpnClientExe);
            }
            else if (userSettings.VpnManager == 1)
            {
                ConnectVPN("l", userSettings.VpnProfileName, userSettings.VpnClientExe);
            }
        }

        public static void DeleteSnapshot()
        {
            try { File.Delete(snapShotFileName); }
            catch { }
        }
    }
}
