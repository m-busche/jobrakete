﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using ControlzEx.Theming;
using System.Threading.Tasks;
using Serilog;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using JsonSettings.Library;
using System.Windows.Media;

namespace MediHomeOffice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private readonly StringWriter _messages;
        private bool firstStartToday;
        private bool updateAvailable = false;
        private bool overdue = false;
        private Updater update;
        private readonly DispatcherTimer workTimer = new DispatcherTimer();
        private readonly DispatcherTimer pauseTimer = new DispatcherTimer();
        private readonly DispatcherTimer saveDumpTimer = new DispatcherTimer();
        private readonly DispatcherTimer connectionCheckTimer = new DispatcherTimer();
        private readonly string dataDirectory;
        private readonly string ahkExe;
        private Schicht schicht;
        private MmPost matterPost;
        private readonly ILogger _loggerTxtbox;
        public static UserSettings usersettings = new UserSettings();

        public MainWindow()
        {
            InitializeComponent();

            usersettings = SettingsBase.Load<UserSettings>();

            // Apply theme
            string t = $"{usersettings.ThemeAccent}.{usersettings.ThemeColor}";
            if (t.Length > 7)
            {
                try
                {
                    ThemeManager.Current.ChangeTheme(Application.Current, t);
                }
                catch { }
            }
            
            dataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\JobRakete";
            if (!Directory.Exists(dataDirectory)) { Directory.CreateDirectory(dataDirectory); }

            ahkExe = @".\AutoHotkeyA32.exe";

            // Let the app only be started once!
#if !DEBUG
            if (Process.GetProcessesByName("MediHomeOffice").Length > 1)
            {
                Process.Start(ahkExe, "JobRaketeRunning.ahk");
                Application.Current.Shutdown();
            }
#endif

            workTimer.Interval = TimeSpan.FromSeconds(1);
            workTimer.Tick += WorkTimer_Tick;
            pauseTimer.Interval = TimeSpan.FromSeconds(1);
            pauseTimer.Tick += PauseTimer_Tick;
            saveDumpTimer.Interval = TimeSpan.FromMinutes(1);
            saveDumpTimer.Tick += SaveDumpTimer_Tick;
            connectionCheckTimer.Interval = TimeSpan.FromMinutes(1);
            connectionCheckTimer.Tick += ConnectionCheckTimer_TickAsync;

            _messages = new StringWriter();
#if DEBUG
            _loggerTxtbox = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.TextWriter(_messages,
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:l}{NewLine}{Exception}"
                    )
                .CreateLogger();
#else
            _loggerTxtbox = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.TextWriter(_messages,
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:l}{NewLine}{Exception}"
                    )
                .CreateLogger();
#endif

            schicht = new Schicht();
            // update = new Updater();
            matterPost = new MmPost();

            lbTimeToWork.Content = schicht.TargetWorkTime.ToString(@"hh\:mm") + " h";
            lbSollzeit.Content = schicht.TargetWorkTime.ToString(@"hh\:mm") + " h";
            lblWorkStart.Content = "";
            lblEndOfWork.Content = "";
            pbPausetime.ToolTip = "0 %";
            pbWorktime.ToolTip = "0 %";

            firstStartToday = Helpers.IsFirststartToday();
            if (firstStartToday)
            {
                _loggerTxtbox.Debug("Erster Start des Tages, du bekommst ein Zitat!");
                txActivity.Text = _messages.ToString();
                _ = ZitatDesTages();
            }
            else { LoadSnapshotAsync(); }
            Updater.DeleteOrphanedUpdates();
        }

        private async void LoadSnapshotAsync()
        {
            try
            {
                Schicht snapschicht = Helpers.ReadFromJsonFile<Schicht>();
                if (snapschicht.ShiftStart.Date == DateTime.Today)
                {
                    _loggerTxtbox.Debug("Vorherige Aufzeichnung gefunden");
                    txActivity.Text = _messages.ToString();
                    MetroDialogSettings settings = new MetroDialogSettings();
                    settings.AffirmativeButtonText = "Ja";
                    settings.NegativeButtonText = "Nein";
                    settings.DefaultButtonFocus = MessageDialogResult.Affirmative;
                    MessageDialogResult result = await this.ShowMessageAsync("Aufzeichnung fortsetzen?",
                        "Ich habe eine vorherige Aufzeichnung deiner Arbeitszeit vom heutigen Datum gefunden, sollen die Daten geladen werden?\n\n" +
                        "Wenn du auf \"Nein\" klickst, gehen die Daten verloren!",
                        MessageDialogStyle.AffirmativeAndNegative, settings: settings);
                    
                    if (result == MessageDialogResult.Affirmative)
                    {
                        schicht = snapschicht;
                        schicht.LoadedFromSnapshot = true;
                        schicht.FeierabendClickedOnce = false;
                        snapschicht.ShiftEnd = DateTime.MinValue;
                        _loggerTxtbox.Information("Vorherige Aufzeichnung wurde geladen");
                        txActivity.Text = _messages.ToString();
                        // Zwischenzeit zuordnen
                        MetroDialogSettings settings2 = new MetroDialogSettings();
                        settings2.AffirmativeButtonText = "Pause";
                        settings2.NegativeButtonText = "Arbeit";
                        string lastStatus;
                        if (schicht.isWorking == true) { lastStatus = "Arbeit"; } else { lastStatus = "Pause"; }
                        TimeSpan delta = DateTime.Now - schicht.CurrentSectionEnd;
                        MessageDialogResult result2 = await this.ShowMessageAsync("Zwischenzeit?", 
                            $"Bei letzten Speichern war dein Status \"{lastStatus}\". Wie soll die seit {schicht.CurrentSectionEnd:HH\\:mm\\:ss} Uhr " + 
                            $"vergangene Zeit von {delta:hh\\:mm\\:ss} (hh:mm:ss) zugeordnet werden?", 
                            MessageDialogStyle.AffirmativeAndNegative,
                            settings:settings2);
                        switch (result2)
                        {
                            // Arbeitszeit
                            case MessageDialogResult.Negative:
                                schicht.CurrentSectionStart = schicht.CurrentSectionEnd;
                                schicht.CurrentSectionEnd = DateTime.Now;
                                schicht.CurrentSectionDuration = schicht.CurrentSectionEnd - schicht.CurrentSectionStart;
                                schicht.CurrentSectionComment = "Nach Unterbrechung hinzugefügte Arbeitszeit";
                                schicht.isWorking = true;
                                Schicht.AddSektion(schicht);
                                _loggerTxtbox.Information("Vorherige Aufzeichnung wurde der Arbeitszeit hinzugefügt");
                                txActivity.Text = _messages.ToString();
                                break;
                            // Pausenzeit
                            case MessageDialogResult.Affirmative:
                                schicht.CurrentSectionStart = schicht.CurrentSectionEnd;
                                schicht.CurrentSectionEnd = DateTime.Now;
                                schicht.CurrentSectionDuration = schicht.CurrentSectionEnd - schicht.CurrentSectionStart;
                                schicht.CurrentSectionComment = "Nach Unterbrechung hinzugefügte Pausenzeit";
                                schicht.isWorking = false;
                                Schicht.AddSektion(schicht);
                                _loggerTxtbox.Information("Vorherige Aufzeichnung wurde der Pausenzeit hinzugefügt");
                                txActivity.Text = _messages.ToString();
                                break;
                            default:
                                break;
                        }
                        SaveDump();
                        Schicht.UpdateTimes(schicht);
                        lblWorkTime.Content = $"🛠 {schicht.TotalWorkTime:hh\\:mm\\:ss}";
                        lblPauseTime.Content = $"☕ {schicht.TotalPauseTime:hh\\:mm\\:ss}";
                        lbTimeToWork.Content = (schicht.TargetWorkTime - schicht.TotalWorkTime).ToString(@"hh\:mm");
                        lblWorkStart.Content = $"🚀 {schicht.ShiftStart:HH\\:mm} Uhr";
                        TimeSpan totalPause = schicht.TotalPauseTime > TimeSpan.FromHours(1) ? schicht.TotalPauseTime : TimeSpan.FromHours(1);
                        lblEndOfWork.Content = $"🥂 {(schicht.ShiftStart + schicht.TargetWorkTime + totalPause):HH\\:mm} Uhr";
                        pbWorktime.Value = schicht.WorkProgress;
                        pbWorktime.ToolTip = $"{schicht.WorkProgress} %";
                        pbPausetime.Value = schicht.PauseProgress;
                        pbPausetime.ToolTip = $"{schicht.PauseProgress} %";
                    }
                }
                Helpers.DeleteSnapshot();
                _loggerTxtbox.Information("Vorherige Aufzeichnung wurde gelöscht");
                txActivity.Text = _messages.ToString();
            }
            catch { return; }
        }

        private void WorkTimer_Tick(object sender, EventArgs e)
        {
            Schicht.UpdateTimes(schicht);
            lblWorkTime.Content = $"🛠 {schicht.TotalWorkTime:hh\\:mm\\:ss}";
            pbWorktime.Value = schicht.WorkProgress;
            pbWorktime.ToolTip = $"{schicht.WorkProgress} %";
            if (!overdue)
            {
                lbTimeToWork.Content = (schicht.TargetWorkTime - schicht.TotalWorkTime).ToString(@"hh\:mm") + " h";
            }
            else
            {
                lbTimeToWork.Content = "-" + (schicht.TargetWorkTime - schicht.TotalWorkTime).ToString(@"hh\:mm") + " h";
            }
            
            if (schicht.TotalWorkTime >= schicht.TargetWorkTime)
            {
                if (!overdue) { Helpers.PlaySound(4); }
                lbTimeToWork.Foreground = Brushes.Red;
                overdue = true;  
            }
        } 
        private void PauseTimer_Tick(object sender, EventArgs e)
        {
            Schicht.UpdateTimes(schicht);
            lblPauseTime.Content = $"☕ {schicht.TotalPauseTime:hh\\:mm\\:ss}";
            pbPausetime.ToolTip = $"{schicht.PauseProgress} %";
            pbPausetime.Value = schicht.PauseProgress;
        }

        private void SaveDumpTimer_Tick(object sender, EventArgs e)
        {
            SaveDump();
        }

        private async void ConnectionCheckTimer_TickAsync(object sender, EventArgs e)
        {
            bool connected = await Helpers.PingHostAsync(usersettings.IP2Connect);
            if (connected)
            {
                _loggerTxtbox.Debug("Verbindungscheck: OK");
                txActivity.Text = _messages.ToString();
                return;
            }
            // Nicht verbunden
            else
            {
                connectionCheckTimer.Stop();
                _loggerTxtbox.Warning("Verbindungscheck: Getrennt");
                txActivity.Text = _messages.ToString();
                for (int i = 1; i < 4; i++)
                {
                    if (usersettings.VpnManager == 0)
                    {
                        Helpers.StartProcess(ahkExe, @".\ShrewBeenden.ahk");
                    }
                    Helpers.StartProcess(ahkExe, @".\SwyxBeenden.ahk");

                    _loggerTxtbox.Information("Warte 1 Minute bis zum erneuten Verbindungsaufbau...");
                    txActivity.Text = _messages.ToString();
                    await Task.Delay(60000);

                    // Neu verbinden
                    _loggerTxtbox.Information("Verbindung wird neu aufgebaut (Versuch {V}/3)", i.ToString());
                    txActivity.Text = _messages.ToString();
                    connected = await ConnectVPN();
                    if (connected)
                    {
                        _loggerTxtbox.Information("Verbindung wurde wieder aufgebaut");
                        txActivity.Text = _messages.ToString();
                        StartSwyxAndRdp();
                        connectionCheckTimer.Start();
                        break;
                    }
                    else
                    {
                        _loggerTxtbox.Information("Verbindung funktioniert nicht");
                        txActivity.Text = _messages.ToString();
                    }
                }
                
                if (usersettings.VpnManager == 0)
                {
                    Helpers.StartProcess(ahkExe, @".\ShrewBeenden.ahk");
                }
                Helpers.StartProcess(ahkExe, @".\SwyxBeenden.ahk");

                _loggerTxtbox.Information("Warte 1 Minute bis zum erneuten Verbindungsaufbau...");
                txActivity.Text = _messages.ToString();
                await Task.Delay(60000);

                // Neu verbinden
                _loggerTxtbox.Information("Verbindung wird neu aufgebaut");
                txActivity.Text = _messages.ToString();
                connected = await ConnectVPN();
                if (connected)
                {
                    StartSwyxAndRdp();
                }
                if (usersettings.VpnManager < 2)
                {
                    connectionCheckTimer.Start();
                }
            }
        }

        private void SaveDump(bool backup = false)
        {
            Schicht.UpdateTimes(schicht);
            Helpers.WriteToJsonFile(schicht, backup: backup);
            lbLastSave.Content = DateTime.Now.ToString(@"HH:mm") + " Uhr";
        }

        private async Task<bool> ConnectVPN()
        {
            // Verbindungsaufbau mit VPN Client
            if (usersettings.VpnManager < 2)
            {
                if (!File.Exists(usersettings.VpnClientExe))
                {
                    _ = this.ShowMessageAsync("Achtung", "VPN Client Datei nicht gefunden, überprüfe deine Einstellungen.", MessageDialogStyle.Affirmative);
                    _loggerTxtbox.Warning("VPN Client exe nicht gefunden");
                    txActivity.Text = _messages.ToString();
                    return false;
                }
                _loggerTxtbox.Information("VPN-Verbindung wird aufgebaut");
                txActivity.Text = _messages.ToString();
                Helpers.ClientConnect(usersettings);
                _loggerTxtbox.Debug("Warte 6 Sekunden auf Verbindungsaufbau");
                txActivity.Text = _messages.ToString();
                await Task.Delay(6000);
            }

            _loggerTxtbox.Information("Verbindung wird überprüft (Ping)");
            txActivity.Text = _messages.ToString();
            return await Helpers.PingHostAsync(usersettings.IP2Connect);
        }

        private async void BtStart_Click(object sender, RoutedEventArgs e)
        {
            if (!schicht.isWorking || schicht.LoadedFromSnapshot)
            {
                bool connected = await ConnectVPN();

                if (connected == true)
                {
                    if (usersettings.VpnManager < 2)
                    {
                        connectionCheckTimer.Start();
                    }
                    _loggerTxtbox.Information("Verbindung funktioniert (Ping)");
                    txActivity.Text = _messages.ToString();
                    schicht.isWorking = true;
                    Helpers.PlaySound(0);
                    saveDumpTimer.Start();
                    SendMM($"{matterPost.username} hat sich angemeldet.");

                    if (!schicht.LoadedFromSnapshot)
                    {
                        schicht.CurrentSectionStart = DateTime.Now;
                    }
                    // Geladene Sitzung fortsetzen
                    else 
                    {
                        schicht.LoadedFromSnapshot = false;
                    }

                    // Arbeitszeit Timer starten
                    workTimer.Start();

                    // Shrew Client minimieren
                    if (usersettings.VpnManager == 0)
                    {
                        _loggerTxtbox.Debug("Minimiere Shrew Client");
                        txActivity.Text = _messages.ToString();
                        Helpers.StartProcess(ahkExe, @".\ShrewMinimize.ahk");
                        await Task.Delay(2000);

                    }
                                        
                    // Ersten Tagesstart speichern
                    if (firstStartToday)
                    {
                        firstStartToday = false;
                        schicht.ShiftStart = DateTime.Now;
                        SaveDump();
                    }

                    // SchichtStart setzen, falls heute noch keine Schicht begonnen wurde
                    if (schicht.ShiftStart.Date < DateTime.Today.Date) { 
                        schicht.ShiftStart = DateTime.Now;
                        SaveDump();
                    }
                    
                    lbStatus.Content = "Du arbeitest 🛠";
                    lblWorkStart.Content = $"🚀 {schicht.ShiftStart:HH\\:mm} Uhr";
                    TimeSpan totalPause = schicht.TotalPauseTime > TimeSpan.FromHours(1) ? schicht.TotalPauseTime : TimeSpan.FromHours(1);
                    lblEndOfWork.Content = $"🥂 {(schicht.ShiftStart + schicht.TargetWorkTime + totalPause):HH\\:mm} Uhr";

                    StartSwyxAndRdp();
                }
                else
                {
                    // Beende Task und verbinde erneut
                    _loggerTxtbox.Information("Verbindung funktioniert nicht (Ping)");
                    txActivity.Text = _messages.ToString();
                    schicht.isWorking = false;
                    workTimer.Stop();
                    lbStatus.Content = "Fehlstart";
                    if (usersettings.VpnManager < 2)
                    {
                        _loggerTxtbox.Information("Beende VPN-Verbindung");
                        txActivity.Text = _messages.ToString();
                    }
                    if (usersettings.VpnManager == 0)
                    {
                        // Shrew Client beenden
                        Helpers.StartProcess(ahkExe, @".\ShrewLogout.ahk");
                    }
                    else if (usersettings.VpnManager == 1)
                    {
                        // Lancom Client Disconnect
                        Helpers.StartProcess(usersettings.VpnClientExe, "/disconnect");
                    }
                    else
                    {
                        await this.ShowMessageAsync("Fehler", "Tut mir leid, die Verbindung funktioniert nicht. " + 
                            "Überprüfe deine Netzwerk- oder VPN-Verbindung bzw. die Einstellung der IP-Adresse.",
                            MessageDialogStyle.Affirmative);
                        lbStatus.Content = "Fehlstart";
                    }
                }
            }
            else
            {
                // Verbindung ist aktiv und funktionabel
            }
        }

        private void StartSwyxAndRdp()
        {
            // Starte Swyx und Remoteverbindung
            if (usersettings.VpnManager < 2)
            {
                _loggerTxtbox.Information("VPN-Verbindung erfolgreich aufgebaut");
                txActivity.Text = _messages.ToString();
            }

            if (usersettings.SwyxStart)
            {
                Helpers.StartProcess(usersettings.SwyxExe);
                _loggerTxtbox.Information("Swyx wurde gestartet");
                txActivity.Text = _messages.ToString();
            }
            if (usersettings.RdpStart)
            {
                Helpers.StartProcess("mstsc.exe", $"/v:{usersettings.IP2Connect}");
                _loggerTxtbox.Information("Remotesitzung wurde gestartet");
                txActivity.Text = _messages.ToString();
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Properties.Settings.Default.Save();
            usersettings.Save();
            if (usersettings.WorkLog == true)
            {
                Feierabend(hardexit: true);
            }
            if (updateAvailable)
            {
                Updater.InstallUpdate(update);
            }
        }

        private async void BtPause_Click(object sender, RoutedEventArgs e)
        {
            if (firstStartToday == true)
            {
                await this.ShowMessageAsync("Ups", "Du kannst erst pausieren nachdem du deinen Arbeitstag bekommen hast. Also bitte erstmal einloggen.", 
                    MessageDialogStyle.Affirmative);
            }

            // Wechsel Arbeitszeit -> Pause
            if (schicht.isWorking == true)
            {
                workTimer.Stop();
                schicht.CurrentSectionComment = "Vollständig protokollierte Arbeitszeit";
                Schicht.UpdateTimes(schicht);
                Schicht.AddSektion(schicht);
                schicht.isWorking = false;
                lbStatus.Content = "Du pausierst ☕";
                pauseTimer.Start();
                Helpers.PlaySound(1);
                ShowPausennachricht();
                _loggerTxtbox.Information("Pause gestartet");
                txActivity.Text = _messages.ToString();
                Thickness t = new Thickness(t.Bottom = 6, t.Left = 6, t.Right = 6, t.Top = 6);
                btPause.BorderThickness = t;
                if (usersettings.SwyxStart == true)
                {
                    Helpers.StartProcess(ahkExe, @".\SwyxLogout.ahk");
                    _loggerTxtbox.Information("Swyx wird abgemeldet");
                    txActivity.Text = _messages.ToString();
                }
            }
            // Wechsel Pause -> Arbeitszeit
            else
            {
                pauseTimer.Stop();
                schicht.CurrentSectionComment = "Vollständig protokollierte Pausenzeit";
                Schicht.UpdateTimes(schicht);
                Schicht.AddSektion(schicht);
                schicht.isWorking = true;
                lbStatus.Content = "Du arbeitest 🛠";
                workTimer.Start();
                Helpers.PlaySound(0);
                SendMM($"{matterPost.username} hat sich nach einer Pause wieder angemeldet.");                
                _loggerTxtbox.Information("Pause beendet");
                txActivity.Text = _messages.ToString();
                Thickness t = new Thickness(t.Bottom = 2, t.Left = 2, t.Top = 2, t.Right = 2);
                btPause.BorderThickness = t;
                if (usersettings.SwyxStart)
                {
                    Helpers.StartProcess(ahkExe, @".\SwyxLogin.ahk");
                    _loggerTxtbox.Information("Swyx wird angemeldet");
                    txActivity.Text = _messages.ToString();
                }
            }
            SaveDump();
        }

        private void BtStop_Click(object sender, RoutedEventArgs e)
        {
            Feierabend();
        }

        private async void Feierabend(bool hardexit = false)
        {
            // Verbindungsüberwachung beenden
            if (usersettings.VpnManager < 2)
            {
                connectionCheckTimer.Stop();
            }

            if (usersettings.WorkLog)
            {
                // Erster Click auf Feierabend: Timer stoppen, Zeiten berechnen und anzeigen
                if (schicht.FeierabendClickedOnce == false)
                {
                    if (!hardexit)
                    {
                        // Ding dong
                        Helpers.PlaySound(2);
                    }

                    schicht.FeierabendClickedOnce = true;

                    // Alle Timer beenden
                    schicht.ShiftEnd = DateTime.Now;
                    workTimer.Stop();
                    pauseTimer.Stop();
                    saveDumpTimer.Stop();

                    // Letzte Sektion hinzufügen
                    schicht.CurrentSectionEnd = DateTime.Now;
                    schicht.CurrentSectionDuration = schicht.CurrentSectionEnd - schicht.CurrentSectionStart;
                    if (schicht.isWorking)
                    {
                        schicht.WorkSections.Add(new TimeSection
                        {
                            Start = schicht.CurrentSectionStart,
                            End = schicht.CurrentSectionEnd,
                            Duration = schicht.CurrentSectionDuration,
                            Comment = "Vollständig protokollierte Arbeitszeit"
                        });
                    }
                    else
                    {
                        schicht.PauseSections.Add(new TimeSection
                        {
                            Start = schicht.CurrentSectionStart,
                            End = schicht.CurrentSectionEnd,
                            Duration = schicht.CurrentSectionDuration,
                            Comment = "Vollständig protokollierte Pausezeit"
                        });
                    }

                    lbStatus.Content = "Feierabend";

                    // Laufende Zeit addieren, dann summieren
                    if (schicht.isWorking)
                    {
                        // Laufende Arbeitszeit addieren
                        schicht.WorkSectionsDurations.Add(schicht.CurrentSectionDuration);
                    }
                    else
                    {
                        // Laufende Pausenzeit addieren
                        schicht.PauseSectionsDurations.Add(schicht.CurrentSectionDuration);
                    }

                    // Nochmal speichern
                    SaveDump();

                    // Backup als Archiv
                    if (usersettings.WorkLog) { SaveDump(backup: true); }

                    // Arbeits- und Pausenzeiten summieren
                    TimeSpan tsWorkTimeSum = schicht.WorkSectionsDurations.Sum();
                    TimeSpan tsPauseTimeSum = schicht.PauseSectionsDurations.Sum();

                    // Loggerei
                    _loggerTxtbox.Information("Feierabend");
                    txActivity.Text = _messages.ToString();

                    // Zusammenfassung nur bei Klick auf Feierabend-Button
                    if (hardexit == false)
                    {
                        await this.ShowMessageAsync("Tageszusammenfassung", $"Du hast heute {tsWorkTimeSum:hh\\:mm\\:ss} gearbeitet und " +
                            $"{tsPauseTimeSum:hh\\:mm\\:ss} Pause gemacht. 🙏🏻\n\n" +
                            $"Beginn des Arbeitstages: {schicht.ShiftStart:HH\\:mm} Uhr\n" +
                            $"Ende des Arbeitstages: {schicht.ShiftEnd:HH\\:mm} Uhr\n" +
                            "Übertrage nun deine Zeiten in \"MediWork\" und klicke anschließend noch einmal auf den Feierabend-Knopf, um VPN und Swyx zu beenden.",
                            MessageDialogStyle.Affirmative);
                    }
                }
                else
                // Zweiter Click auf Feierabend: Verbindung beenden, Swyx und VPN beenden
                {
                    _loggerTxtbox.Information("Beende Swyx- und VPN Client");
                    txActivity.Text = _messages.ToString();
                    Helpers.StartProcess(ahkExe, @".\SwyxBeenden.ahk");
                    if (usersettings.VpnManager == 0)
                    {
                        Helpers.StartProcess(ahkExe, @".\ShrewLogout.ahk");
                    }
                    btStart.IsEnabled = false;
                    btPause.IsEnabled = false;
                    btStop.IsEnabled = false;
                }
            }
        }
        private void BtOpenLogDir_Click(object sender, RoutedEventArgs e)
        {
            Helpers.StartProcess("explorer.exe", dataDirectory);
        }

        private void BtMattermost_Click(object sender, RoutedEventArgs e)
        {
            Helpers.StartShellProcess("https://mattermost.medisoftware.org/medisoftware/");
        }

        private void MnSettings_Click(object sender, RoutedEventArgs e)
        {
            Settings subWindow = new Settings();
            subWindow.ShowDialog();
            matterPost = new MmPost();
            // TODO: Geänderte Einstellungen neu sezten?
            lbTimeToWork.Content = schicht.TargetWorkTime.ToString(@"hh\:mm") + " h";
            lbSollzeit.Content = schicht.TargetWorkTime.ToString(@"hh\:mm") + " h";
        }

        private void MnExit_Click(object sender, RoutedEventArgs e)
        {
            Feierabend(true);
            if (updateAvailable)
            {
                Updater.InstallUpdate(update);
            }
            Application.Current.Shutdown();
        }

        private void BtAbout_Click(object sender, RoutedEventArgs e)
        {
            About subWindow = new About();
            subWindow.ShowDialog();
        }

        private void TxActivity_TextChanged(object sender, TextChangedEventArgs e)
        {
            txActivity.ScrollToEnd();
        }

        private async Task ZitatDesTages()
        {
            Zitate_Online.Zitat z = Zitate_Online.Zitate.GetZitat();
            await this.ShowMessageAsync("Zitat des Tages",
                $"{z.ZitatText}\n\n({z.ZitatAuthor})\n\nQuelle: https://www.zitate-online.de/",
                MessageDialogStyle.Affirmative);
        }

        //private async void Welcome()
        //{
        //    await this.ShowMessageAsync("Wichtige Hinweise", "Hallo! Du benutzt die JobRakete zum ersten Mal. Daher ein paar Tipps:\n" +
        //        "- Überprüfe die Allgemeinen Einstellungen (Datei -> Einstellungen -> Allgemein).\n" +
        //        "- Falls du nicht 8 Stunden/Tag arbeitest: Trage deine täglichen Arbeitszeiten unter Datei -> Einstellungen -> Arbeitszeiten ein.\n\n" +
        //        "Wichtig: Die JobRakete ersetzt nicht unsere Arbeitszeitverwaltung, sie liefert dir aber die Werte, die du dort eintragen kannst. " +
        //        "Zum Feierabend musst du also deine Zeiten noch in \"MediWork\" übertragen.", MessageDialogStyle.Affirmative);
        //}

        private async void ShowPausennachricht()
        {
            if (matterPost.activated)
            {
                MetroDialogSettings settings = new MetroDialogSettings();
                settings.AffirmativeButtonText = "Nachricht Senden";
                settings.NegativeButtonText = "Nichts senden";
                settings.DefaultButtonFocus = MessageDialogResult.Negative;
                settings.DefaultText = $"{matterPost.username}: Ich mache jetzt eine ☕/🍕/🍰 Pause (x Minuten).";
                var result = await this.ShowInputAsync("Abwesenheitsnachricht", "Mattermost Abwesenheitsnachricht (dein Name sollte in der Nachricht stehen!)", settings);
                if (result == null)
                {
                    return;
                }
                else
                {
                    SendMM(result);
                }
            }
        }

        private void SendMM(string text)
        {
            if (matterPost.activated)
            {
                _loggerTxtbox.Information("Sende Mattermost Nachricht");
                txActivity.Text = _messages.ToString();
                matterPost.text = text;
                MmPost.Send(matterPost);
            }
        }

        private void MnChkUpd_Click(object sender, RoutedEventArgs e)
        {
            update = Updater.CheckUpdate();
            if (update != null)
            {
                _loggerTxtbox.Information("Updatecheck: Ein Update steht bereit");
                txActivity.Text = _messages.ToString();
                _ = this.ShowMessageAsync("Hinweis", "Es steht eine neue Version zum Download bereit. " +
                    "Das Update wird im Hintergrund herunter geladen und beim Beenden der App installiert.",
                    MessageDialogStyle.Affirmative);
                Updater.DownloadSetup(update);
                updateAvailable = true;
            }
            else
            {
                _loggerTxtbox.Information("Updatecheck: Kein Update verfügbar");
                txActivity.Text = _messages.ToString();
                _ = this.ShowMessageAsync("Hinweis", "Du hast bereits die neueste Version installiert.",
                    MessageDialogStyle.Affirmative);
            }
        }

        private void MnReadme_Click(object sender, RoutedEventArgs e)
        {
            Helpers.StartShellProcess("JobRakete.pdf");
        }
    }
}
