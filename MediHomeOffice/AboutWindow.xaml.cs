﻿using MahApps.Metro.Controls;
using System;
using System.Reflection;

namespace MediHomeOffice
{
    /// <summary>
    /// Interaktionslogik für About.xaml
    /// </summary>
    public partial class About : MetroWindow
    {
        public About()
        {
            InitializeComponent();
            Version version = Assembly.GetEntryAssembly().GetName().Version;
            lbProgrammversion.Content = $"JobRakete Version {version}";
        }

        private void Label_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Helpers.StartShellProcess("https://jobrakete.eu/");
        }
    }
}
