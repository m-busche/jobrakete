﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace MediHomeOffice
{
    class Updater
    {
        private static readonly string jsonUrl = "https://jobrakete.eu/jobrakete/info.json";
        public Version LocalVersion { get; set; }
        public Version RemoteVersion { get; set; }
        public string DownloadUrl { get; set; }
        public string SetupFileName { get; set; }

        /// <summary>
        /// Download remote update information
        /// </summary>
        /// <returns></returns>
        private static Updater GetInfo()
        {
            string json = new WebClient().DownloadString(jsonUrl);

            Updater u = JsonConvert.DeserializeObject<Updater>(json);
            u.LocalVersion = Assembly.GetEntryAssembly().GetName().Version;
            return u;
        }

        public static void DownloadSetup(Updater u)
        {
            string fileName = Path.GetTempPath() + u.SetupFileName;
            using WebClient myWebClient = new WebClient();
            // Download the Web resource and save it into the current filesystem folder.
            myWebClient.DownloadFileCompleted += wc_DownloadFileCompleted;
            myWebClient.DownloadFile(new Uri(u.DownloadUrl), fileName);
            return;
        }

        private static void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            // Install update
            Console.WriteLine("Download completed.");
        }

        public static Updater CheckUpdate()
        {
            Updater u = new Updater();
            u = GetInfo();

            if (u.RemoteVersion.CompareTo(u.LocalVersion) > 0)
            {
                u.SetupFileName = $"setupJobRakete_{u.RemoteVersion}.exe";
                return u;
            }
            else 
            {
                return null;
            }
        }

        public static void InstallUpdate(Updater u)
        {
            Process p = new Process();
            p.StartInfo.FileName = Path.GetTempPath() + u.SetupFileName;
            p.StartInfo.Arguments = "/SILENT /SP- /CLOSEAPPLICATIONS /NORESTART";
            try
            {
                p.Start();
            }
            catch { }
        }

        public static void DeleteOrphanedUpdates()
        {
            foreach (string f in Directory.EnumerateFiles(Path.GetTempPath(), "setupJobRakete_*.exe"))
            {
                try
                {
                    File.Delete(f);
                }
                catch { }
            }
        }
    }
}
