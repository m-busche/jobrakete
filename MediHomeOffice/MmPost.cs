﻿using System.Threading.Tasks;
using Matterhook.NET.MatterhookClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediHomeOffice
{
    class MmPost
    {
        public string webhookUrl { get; set; }
        public string webhookUser { get; set; }
        public string channel { get; set; } = "jobrakete";
        public string iconurl { get; set; }
        public string username { get; set; }
        public string text { get; set; } = "Hallo Welt";
        public bool activated { get; set; }
        public static void Send(MmPost post)
        {
            var client = new MatterhookClient(post.webhookUrl);
            var message = new MattermostMessage()
            {
                //MessageOptions
                Text = post.text,
                Channel = post.channel,
                IconUrl = post.iconurl
            };
            // Task.WaitAll(client.PostAsync(message));
            _ = client.PostAsync(message);
        }

        public MmPost()
        {
            webhookUrl= MainWindow.usersettings.MmWebhook;
            webhookUser = MainWindow.usersettings.MmWebhookUser;
            channel = MainWindow.usersettings.MmChannel;
            iconurl = MainWindow.usersettings.MmIconUrl;
            activated = MainWindow.usersettings.MmActivated;
            username = MainWindow.usersettings.MmUserName;
        }
    }
}
