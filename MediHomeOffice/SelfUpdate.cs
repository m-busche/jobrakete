﻿using NetSparkleUpdater;
using NetSparkleUpdater.Enums;
using NetSparkleUpdater.SignatureVerifiers;
using NetSparkleUpdater.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediHomeOffice
{
    class SelfUpdate
    {
        private SparkleUpdater _sparkle;
        public void CheckUpdate()
        {
            _sparkle = new SparkleUpdater(
                "http://example.com/appcast.xml", // link to your app cast file
                new Ed25519Checker(SecurityMode.Strict, // security mode -- use .Unsafe to ignore all signature checking (NOT recommended!!)
                "base_64_public_key") // your base 64 public key -- generate this with the NetSparkleUpdater.Tools AppCastGenerator on any OS
)
            {
                // UIFactory = new NetSparkleUpdater.UI.WPF.UIFactory(icon) // or null or choose some other UI factory or build your own!
            };
            _sparkle.StartLoop(true); // `true` to run an initial check online -- only call StartLoop once for a given SparkleUpdater instance!
        }
    }
}
