﻿// https://stackoverflow.com/questions/4703046/sum-of-timespans-in-c-sharp
using System;
using System.Collections.Generic;

namespace MediHomeOffice
{
    public static class StatisticExtensions
    {
        public static TimeSpan Sum(this IEnumerable<TimeSpan> timeSpans)
        {
            TimeSpan sumTillNowTimeSpan = TimeSpan.Zero;
            foreach (TimeSpan timeSpan in timeSpans)
            {
                sumTillNowTimeSpan += timeSpan;
            }
            return sumTillNowTimeSpan;
        }
    }
}
