﻿using System;

namespace MediHomeOffice
{
    class Snapshot
    {
        private TimeSpan tswork;
        private TimeSpan tspause;
        private bool valid;
        private string laststate;
        private TimeSpan dtTerminated;

        public TimeSpan WorkTs
        {
            get { return tswork; }
            set { tswork = value; }
        }

        public TimeSpan PauseTs
        {
            get { return tspause; }
            set { tspause = value; }
        }

        public bool Valid
        {
            get { return valid; }
            set { valid = value; }
        }

        public string Laststate
        {
            get { return laststate; }
            set { laststate = value; }
        }

        public TimeSpan Terminated
        {
            get { return dtTerminated; }
            set { dtTerminated = value; }
        }
    }
}
